package Task4;

public class OddNumberException extends Exception{
    private final int number;

    public OddNumberException(int number) {
        super("Нечетное число " + number);
        this.number = number;
    }

    public double getNumber() {
        return number;
    }
}
