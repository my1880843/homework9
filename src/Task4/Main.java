package Task4;

public class Main {
    public static void main(String[] args) {
        try {
            var numb1 = new MyEvenNumber(12);
            var numb2 = new MyEvenNumber(13);
        } catch (OddNumberException e) {
            throw new RuntimeException(e);
        }
    }
}
