package Task4;

public class MyEvenNumber {
    private int n;

    public MyEvenNumber(int n) throws OddNumberException {
        setN(n);
    }

    public int getN() {
        return n;
    }

    public void setN(int n) throws OddNumberException {
        if (n % 2 != 0)
            throw new OddNumberException(n);
        else
            this.n = n;
    }
}
