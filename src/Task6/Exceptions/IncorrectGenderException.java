package Task6.Exceptions;

import Task6.Enums.ExceptionMessage;

public class IncorrectGenderException extends Exception{
    public IncorrectGenderException() {
        super(ExceptionMessage.INCORRECT_GENDER.toString());
    }
}
