package Task6.Exceptions;

import Task6.Enums.ExceptionMessage;

public class IncorrectNameException extends Exception{
    public IncorrectNameException(){
        super(ExceptionMessage.INCORRECT_NAME.toString());
    }
}
