package Task6.Exceptions;

import Task6.Enums.ExceptionMessage;

public class IncorrectBirthDateException extends Exception{
    public IncorrectBirthDateException(){
        super(ExceptionMessage.INCORRECT_BIRTHDATE.toString());
    }
}
