package Task6.Exceptions;

import Task6.Enums.ExceptionMessage;

public class IncorrectHeightException extends Exception{
    public IncorrectHeightException() {
        super(ExceptionMessage.INCORRECT_HEIGHT.toString());
    }
}
