package Task6;

import Task6.Enums.Genders;
import Task6.Exceptions.IncorrectBirthDateException;
import Task6.Exceptions.IncorrectGenderException;
import Task6.Exceptions.IncorrectHeightException;
import Task6.Exceptions.IncorrectNameException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormValidator {
    private FormValidator() {
    }

    public static void checkName(String str) throws IncorrectNameException {
        if (str.length() < 2 || str.length() > 20 || Character.isLowerCase(str.charAt(0))) {
            throw new IncorrectNameException();
        }
    }

    public static void checkBirthdate(String str) throws ParseException, IncorrectBirthDateException {
        Date date = new SimpleDateFormat("dd.MM.yyyy").parse(str);
        Date dateFrom = new SimpleDateFormat("dd.MM.yyyy").parse("01.01.1900");
        Date dateTo = new Date(System.currentTimeMillis());

        if (date.after(dateTo) || date.before(dateFrom)) {
            throw new IncorrectBirthDateException();
        }
    }

    public static void checkGender(String str) throws IncorrectGenderException {
        for (var gender : Genders.values()) {
            if (str.equals(gender.toString()))
                return;
        }

        throw new IncorrectGenderException();
    }

    public static void checkHeight(String str) throws IncorrectHeightException {
        double height;

        try {
            height = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            throw new IncorrectHeightException();
        }

        if (height < 0)
            throw new IncorrectHeightException();
    }
}
