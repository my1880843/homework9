package Task6;

import Task6.Exceptions.IncorrectBirthDateException;
import Task6.Exceptions.IncorrectGenderException;
import Task6.Exceptions.IncorrectHeightException;
import Task6.Exceptions.IncorrectNameException;

import java.text.ParseException;

public class Main {
    public static void main(String[] args) {
        try {
            FormValidator.checkBirthdate("01.01.1899");
        } catch (ParseException | IncorrectBirthDateException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkGender("Средний");
        } catch (IncorrectGenderException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkHeight("242б234,234,243");
        } catch (IncorrectHeightException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkName("иван");
        } catch (IncorrectNameException e) {
            System.out.println(e.getMessage());
        }
    }
}
