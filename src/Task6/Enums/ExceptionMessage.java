package Task6.Enums;

public enum ExceptionMessage {
    INCORRECT_BIRTHDATE("Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты."),
    INCORRECT_GENDER("Пол должен корректно матчится в enum Gender, хранящий Male и Female значения."),
    INCORRECT_HEIGHT("Рост должен быть положительным числом и корректно конвертироваться в double."),
    INCORRECT_NAME("Длина имени должна быть от 2 до 20 символов, первая буква заглавная.");
    private final String message;

    ExceptionMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
