package Task6.Enums;

public enum Genders {
    MALE("Мужской"),
    FEMALE("Женский");
    private final String description;

    Genders(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
