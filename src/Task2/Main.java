package Task2;

public class Main {
    public static void main(String[] args) {
        try {
            ThrowMyUncheckedException();
        } catch (MyUncheckedException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void ThrowMyUncheckedException() {
        throw new MyUncheckedException();
    }
}
