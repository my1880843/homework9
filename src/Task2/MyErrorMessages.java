package Task2;

public enum MyErrorMessages {
    DEFAULT_UNCHECKED_MESSAGE("Мое непроверяемое исключение");
    private final String message;

    MyErrorMessages(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
