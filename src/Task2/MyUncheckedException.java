package Task2;

public class MyUncheckedException extends RuntimeException {
    public MyUncheckedException() {
        super(MyErrorMessages.DEFAULT_UNCHECKED_MESSAGE.toString());
    }

    public MyUncheckedException(String message) {
        super(message);
    }
}
