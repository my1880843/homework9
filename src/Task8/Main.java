package Task8;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String s1 = "123321";
        String s2 = "321 123";

        System.out.println(isAnagram(s1, s2));
    }

    public static boolean isAnagram(String s1, String s2) {
        Map<Character, Integer> map1 = getMapFromString(s1);
        Map<Character, Integer> map2 = getMapFromString(s2);

        for (Character key : map1.keySet()) {
            if (!map2.containsKey(key)) {
                return false;
            } else {
                var value1 = map1.get(key);
                var value2 = map2.get(key);

                if (!value1.equals(value2))
                    return false;
            }
        }

        return true;
    }

    private static Map<Character, Integer> getMapFromString(String str) {
        Map<Character, Integer> map = new HashMap<>();

        for (char symbol : str.replaceAll(" ", "").toCharArray()) {
            if (map.containsKey(symbol)) {
                var value = map.get(symbol);
                map.put(symbol, ++value);
            } else {
                map.put(symbol, 1);
            }
        }

        return map;
    }
}
