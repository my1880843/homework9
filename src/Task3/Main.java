package Task3;

import java.io.IOException;

public class Main {
    public static final String INPUTFILE_PATH = "src/Task3/input.txt";
    public static final String OUTPUTFILE_PATH = "src/Task3/output.txt";

    public static void main(String[] args) {
        try {
            createInputFileWithText();
            TextTransferer.fromInputToOutput(INPUTFILE_PATH, OUTPUTFILE_PATH);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void createInputFileWithText() throws IOException {
        String text = "руссКийТекст EnglishText 43534";
        TextTransferer.writeTextToFile(text, INPUTFILE_PATH);
    }
}
