package Task3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TextTransferer {
    private TextTransferer() {

    }

    /**
     * Переносит тескт из файла, путь которого <code>inputPath</code> в файл,
     * путь которого <code>outputPath</code>,
     * с заменой строчных латинских символов на заглавные.
     *
     * @param inputPath  путь к файлу для чтения.
     * @param outputPath путь к фалу для записи.
     */
    public static void fromInputToOutput(String inputPath, String outputPath) throws IOException {
        String inputText = getTextFromFile(inputPath);
        String outputText = changeLatinLowToUpper(inputText);
        writeTextToFile(outputText, outputPath);
    }

    private static String changeLatinLowToUpper(String text) {
        StringBuilder sb = new StringBuilder();

        for (char symbol : text.toCharArray()) {
            var symbolToAppend = symbol;
            for (char letter = 'a'; letter <= 'z'; letter++) {
                if (symbol == letter) {
                    symbolToAppend = Character.toUpperCase(symbol);
                    break;
                }
            }
            sb.append(symbolToAppend);
        }
        return sb.toString();
    }

    public static void writeTextToFile(String text, String fileName) throws IOException {
        try (FileWriter writer = new FileWriter(fileName)) {
            writer.write(text);
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    private static String getTextFromFile(String path) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();

        try (Scanner scanner = new Scanner(new File(path))) {
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine()).append("\n");
            }
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getMessage());
        }

        return sb.toString();
    }
}
