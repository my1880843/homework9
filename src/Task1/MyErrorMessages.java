package Task1;

public enum MyErrorMessages {
    DEFAULT_CHECKED_MESSAGE("Мое проверяемое исключение");
    private final String message;

    MyErrorMessages(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
