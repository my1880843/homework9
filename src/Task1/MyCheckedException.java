package Task1;

public class MyCheckedException extends Exception {
    public MyCheckedException() {
        super(MyErrorMessages.DEFAULT_CHECKED_MESSAGE.toString());
    }

    public MyCheckedException(String message) {
        super(message);
    }
}
