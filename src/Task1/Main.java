package Task1;

public class Main {
    public static void main(String[] args) {
        try {
            ThrowMyCheckedException();
        } catch (MyCheckedException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void ThrowMyCheckedException() throws MyCheckedException {
        throw new MyCheckedException();
    }
}
