package Task10;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<Document> list = getTestDocuments();
        Map<Integer,Document> map = organizeDocuments(list);
        System.out.println(map);
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();

        for (Document document : documents) {
            map.put(document.id, document);
        }

        return map;
    }

    private static ArrayList<Document> getTestDocuments(){
        ArrayList<Document> list = new ArrayList<>();
        var rnd = new Random();

        for (int i = 0; i < 10; i++) {
            var document = new Document();
            document.id = i;
            document.name = "document_"+i;
            document.pageCount = rnd.nextInt(300);
            list.add(document);
        }

        return list;
    }
}
