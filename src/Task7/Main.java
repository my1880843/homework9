package Task7;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> list= new ArrayList<>();
        list.add(1);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(3);

        var unicList = getUnicFromList(list);

        System.out.println(unicList);
    }

    public static <T> Set<T> getUnicFromList(ArrayList<T> list){
        return new HashSet<T>(list);
    }
}
