package Task9;

import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        HashSet<Integer> set1 = new HashSet<>();
        HashSet<Integer> set2 = new HashSet<>();

        set1.add(0);
        set1.add(2);
        set1.add(3);
        set1.add(6);

        set2.add(1);
        set2.add(2);
        set2.add(3);
        set2.add(4);
        set2.add(5);

        System.out.println("intersection " + PowerfulSet.intersection(set1,set2));
        System.out.println("union " + PowerfulSet.union(set1,set2));
        System.out.println("relativeComplement " + PowerfulSet.relativeComplement(set1,set2));
    }
}
