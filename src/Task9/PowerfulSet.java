package Task9;

import java.util.Set;

public class PowerfulSet {
    private PowerfulSet() {
    }

    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        set1.retainAll(set2);
        return set1;
    }

    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        set1.addAll(set2);
        return set1;
    }

    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        set1.removeAll(set2);
        return set1;
    }
}
